EAPI=6

PYTHON_COMPAT=( python3_{4,5,6} )
inherit git-r3 distutils-r1

DESCRIPTION="A Python framework for building 3D printing related applications"
HOMEPAGE="https://code.alephobjects.com/source/numpy-stl"
EGIT_REPO_URI="https://code.alephobjects.com/source/numpy-stl.git"
EGIT_BRANCH="master"
KEYWORDS="~amd64 ~x86"

LICENSE="AGPL-3+"
SLOT="0"
IUSE="test"

RDEPEND="${PYTHON_DEPS}
	dev-python/numpy[${PYTHON_USEDEP}]"
DEPEND="${RDEPEND}
	test? ( dev-python/pytest[${PYTHON_USEDEP}] )"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"
